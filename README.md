## todo-list-html

Template HTML ini akan digunakan sebagai template untuk aplikasi Todo List.

### Kebutuhan
1. Git
2. npm (Node.js)

### Instalasi
1. Clone repository ini
2. Jalankan perintah `npm i` untuk menginstal dependency
3. Buka `index.html` di browser
